class Account extends GenericAccount {

    /** Called once when the Addon is loaded at Thunderbird startup. The account is load()ed before and store()d afterwards.
     * @returns {Promise<GenericAccount>} The Object itself, for chaining
     */
    async setup() {
        return this;
    }

    /** Return an object containing the authentication header(s) needed for communication with the service */
    headers() {
        if (this.username && this.password) {
            return Object.assign(this._headers,
                {
                    "Authorization": "Basic " + btoa(this.username + ':' + this.password),
                });
        } else {
            return this._headers;
        }
    }

    /** Refresh or get an authentication token with the known credentials */
    async refresh_token() {
        return true;
    }
}

/* globals GenericAccount */
/* exported Account */