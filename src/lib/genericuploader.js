class GenericUploader {

    /**
     * @param {string} accountId The id of the account as supplied by Thunderbird
     * @param {CloudFile} fileInfo As supplied by Thunderbird in the event
     */
    constructor(accountId, fileInfo) {
        this.account = new Account(accountId);
        this.uploadId = fileInfo.id;
        this.fileName = fileInfo.name;
        this.fileObject = fileInfo.data;
    }

    /**
     * @returns {Promise<{aborted: boolean, url: string}>} 
     */
    async uploadFile() {
        /* Prepare tracking the status */
        this.uploadStatus = new UploadStatus(this.fileName);
        allUploadStatus.set(this.uploadId, this.uploadStatus);

        // TODO Check if this provider can handle the filename

        const fullPath = this.account.storageFolder + "/" + makeUniqueFolderName(this.fileObject) + "/" + this.fileName;

        /* Make sure the file exists in the cloud */
        if (!await this.findOrUploadFile(fullPath)) {
            return { aborted: true, };
        }

        /* Make sure the file is shared and get the link */
        const url = await this.findOrCreateShareLink(fullPath);

        if (url) {
            /* Everything worked, now the status is only needed to show the download password */
            if (this.uploadStatus.download_password === "") {
                allUploadStatus.delete(this.uploadId);
            }
            return { url, };
        }
        return { aborted: true, };

        /**
         * Make a most probably unique folder name from last modified timestamp and size of fileObject
         * @returns {string} 16 hex characters containing last modified time and size
         */
        function makeUniqueFolderName(fileObject) {
            return fileObject.lastModified.toString(16).toUpperCase().padStart(11, "0") +
                fileObject.size.toString(16).toUpperCase().padStart(14, "0");

        }
    }

    /**
     * Make sure the file exists in the cloud
     * @param {string} fullPath 
     * @returns {Promise<boolean>} Does the file exist in the cloud?
     */
    async findOrUploadFile(fullPath) {
        /* Check if the same file is in the cloud already */
        const stat = await this.getRemoteFileInfo(fullPath);
        if (stat) {
            if (this.filesMatch(stat, this.fileObject)) {
                return true;
            } else {
                /* There is a different file of the same name in the wrong place */
                // TODO set status or handle the error
                return false;
            }
        }

        /* The file is not there, check if we can upload it */
        if (await this.fileTooBig()) {
            return false;
        }
        if ((await this.account.freeSpace()) < this.fileObject.size) {
            return false;
        }
        /* Prepare the upload target folder */
        if (!await this.findOrCreateLibrary(this.account.library)) {
            return false;
        }
        if (!await this.recursivelyCreatePath(fullPath)) {
            return false;
        }

        if (this.actuallyDoUploadFile(fullPath)) {
            this.setMtime(fullPath, this.fileObject.lastModified);
            return true;
        }
        return false;
    }

    /**
     * @param {string} fullPath The full path of the remote file
     * @returns {Promise<boolean>} The path (w/o the file name) exists
     */
    async recursivelyCreatePath(fullPath) {
        const parts = fullPath.split("/");
        for (let i = 2; i < parts.length; i++) {
            const folder = parts.slice(0, i).join("/");
            if (!await this.findOrCreateFolder(folder)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @returns {Promise<boolean>} Is the file bigger than the limit of this account? 
     */
    async fileTooBig() {
        const accountInfo = await messenger.cloudFile.getAccount(this.account.accountId);
        return accountInfo.uploadSizeLimit &&
            accountInfo.uploadSizeLimit > 0 &&
            accountInfo.uploadSizeLimit < this.fileObject.size;
    }

    /**
     * Get an existing or new download link for the file
     * @param {string} fullPath The path of the remote file
     * @returns {Promise<?string>} The download link
     */
    async findOrCreateShareLink(fullPath) {
        const exitingUrl = await this.getFirstSimilarShareOf(fullPath);
        if (exitingUrl) {
            return exitingUrl;
        }

        return this.createNewShareLink(fullPath);
    }

}

/* globals Account, UploadStatus, allUploadStatus */
/* exported GenericUploader */
