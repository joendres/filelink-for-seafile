class GenericAccount {

    /**
     * @param {string} accountId The id as supplied by Thunderbird
     */
    constructor(accountId) {
        this.accountId = accountId;
    }

    /**
     * Load account state from configuration storage
     * @returns {Promise<GenericAccount>} The Object itself, for chaining
     */
    async load() {
        const id = this.accountId;
        const accountInfo = await browser.storage.local.get(id);
        for (const key in accountInfo[id]) {
            this[key] = accountInfo[id][key];
        }
        return this;
    }

    /**
     * Store the current values of all properties in the local browser storage
     * @returns {Promise<GenericAccount>} The Object itself, for chaining
     */
    async store() {
        browser.storage.local.set({ [this.accountId]: this, });
        return this;
    }

    /**
     * Initialize a new account before it is shown in the management page for the first time, eg. set default values
     * @returns {Promise<GenericAccount>} The Object itself, for chaining
     */
    async setupNewAccount() {
        // TODO are these wise choices?
        this.library = null;
        this.storageFolder = "/";

        const manifest = browser.runtime.getManifest();
        this._headers = {
            "User-Agent": manifest.name.replace(
                /^__MSG_([@\w]+)__$/, (matched, key) => {
                    return browser.i18n.getMessage(key) || matched;
                }) + "/" + manifest.version,
        };

        return this;
    }

    /**
     * Clean up when an account is deleted
     */
    async deleteAccount() {
        browser.storage.local.remove(this.accountId);
    }

    /**
     * @returns {Promise<number>}
     */
    async freeSpace() {
        // TODO where to update from cloud?
        const accountInfo = await messenger.cloudFile.getAccount(this.accountId);
        return (accountInfo.spaceRemaining && accountInfo.spaceRemaining >= 0) ? accountInfo.spaceRemaining : Infinity;
    }
}

/* exported GenericAccount */