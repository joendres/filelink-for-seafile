class GenericUploadStatus {

    /**
     * @param {string} filename 
     */
    constructor(filename) {
        this.filename = filename;
        this.status = "preparing";
        this.progress = 0;
        this.error = false;
        this.download_password = "";
    }

    /**
     * 
     * @param {XMLHttpRequest} uploadRequest 
     */
    async uploadStarted(uploadRequest) {
        this.uploadRequest = uploadRequest;
        this.status = "uploading";
    }

    async abort() {
        if (this.uploadRequest && this.uploadRequest.abort) {
            this.uploadRequest.abort();
        }
    }
}

/* exported GenericUploadStatus */