/**
 * 
 * @param {string} endpoint The url of the endpoint, relative to the account's base url
 * @param {string} method HTTP methos
 * @param {*} body The Body for POST/PUT
 * @param {*} moreHeaders Headers specific for this call
 * @returns {Promise<Response>} The response with the parsed json data in the data property
 */
async doJsonCall(account, endpoint, method = "GET", body = "", moreHeaders = {}) {
    const url = account.restUrl + endpoint;
    const headers = Object.assign(account.headers(), moreHeaders);
    const credentials = "omit";

    const init = {
        method,
        headers,
        body,
        credentials,
    };

    const response = await fetch(url, init);
    try {
        response.data = await(await response).json();
    } catch (e) {
        response.data = {};
    }
    return response;
}


/* exported doJsonCall */