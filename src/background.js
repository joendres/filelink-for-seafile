/* Initialize all configured accounts when the Addon is loaded */
browser.storage.local.get().then(
    allAccounts => {
        for (const accountId in allAccounts) { setupAccount(accountId); }
    });

/* Fired when a cloud file account of this add-on was deleted */
messenger.cloudFile.onAccountDeleted.addListener(accountId => {
    const acc = new Account(accountId);
    acc.deleteAccount();
});

/* Fired when a cloud file account of this add-on was created before the management page is opened */
messenger.cloudFile.onAccountAdded.addListener(async accountId => {
    const acc = new Account(accountId);
    acc.setupNewAccount();
    acc.store();
});

/* A Map to keep track of all current uploads */
const allUploadStatus = new Map();

/* Fired when a file should be uploaded to the cloud file provider */
messenger.cloudFile.onFileUpload.addListener(async (account, fileInfo) => {
    const uploader = new Uploader(account.id, fileInfo);
    return uploader.uploadFile();
});

/* Fired when a file uploaded is aborted */
messenger.cloudFile.onFileUploadAbort.addListener(
    (acc, uploadId) => {
        const uploadStatus = allUploadStatus.get(uploadId);
        if (uploadStatus) {
            uploadStatus.abort();
        }
        allUploadStatus.delete(uploadId);
    });

/* Fired when a file previously uploaded should be deleted
   This Addon doesn't delete files because we want to reuse uploads. */
messenger.cloudFile.onFileDeleted.addListener(
    (acc, uploadId) => {
        allUploadStatus.delete(uploadId);
    });

/* This is called for every account on Thunderbird startup */
async function setupAccount(accountId) {
    const acc = new Account(accountId);
    await acc.load();
    await acc.setup();
    acc.store();
}

/* Make jshint happy */
/* global Account, Uploader */