class Uploader extends GenericUploader {

    /**
     * Get properties of a remote file
     * @param {string} fullPath The complete path of the file
     * @returns {Promise<?{size: number, mtime: number}>} An object containing at least size (in bytes) and mtime (in the chosen time unit), null on error
     */
    async getRemoteFileInfo(fullPath) {
        return null;
    }

    /**
     * Check if the upload file is the same as the remote file
     * @param {*} stat An object with remote files properties
     * @param {File} fileObject The upload file
     * @returns {boolean} Does fileObject match the remote file
     */
    filesMatch(stat, fileObject) {
        /* Make sure to adapt to chosen time unit (s vs. ms) */
        return stat.mtime === fileObject.lastModified &&
            stat.size === fileObject.size;
    }

    /**
     * Create the target library configured in the account if it doesn't exist already
     * @returns {Promise<boolean>} The library exists
     */
    async findOrCreateLibrary(library) {
        return true;
    }

    /**
     * Create a folder if it doesn't exist
     * @param {string} folder A full path to a folder, starting with /
     * @returns {Promise<boolean>} The folder exists
     */
    async findOrCreateFolder(folder) {
        return false;
    }

    /**
     * @param {string} fullPath The remote path of the file
     * @returns {Promise<boolean>} Did the upload succeed?
     */
    async actuallyDoUploadFile(fullPath) {
        return false;
    }

    /**
     * Try to set the mtime of the remote file. This is best effort, so don't return a status.
     * @param {string} fullPath The remote path of the file
     * @param {number} mtime The new mtime in the time unit you choose
     * @returns {Promise<boolean>} Mtime was set
     */
    async setMtime(fullPath, mtime) {
        return false;
    }

    /**
     * 
     * @param {string} fullPath 
     * @returns {Promise<?string>}
     */
    async getFirstSimilarShareOf(fullPath) {
        const exitingShares = this.getAllSharesOf(fullPath);
        return null;
    }

    /**
     * 
     * @param {string} fullPath 
     * @returns {Promise<?string>}
     */
    async createNewShareLink(fullPath) {
        return null;
    }

}

/* globals GenericUploader */
/* exported Uploader */