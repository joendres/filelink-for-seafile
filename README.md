# Filelink for Seafile

This Thunderbird (68+) Add On makes it easy to send large attachments: It uploads the files to a Seafile instance and inserts download links into the message.